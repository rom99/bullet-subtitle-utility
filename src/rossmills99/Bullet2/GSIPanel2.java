/**
 * 
 */
package rossmills99.Bullet2;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author rossmills
 *
 */
public class GSIPanel2 extends JPanel {
	private static final int fieldWidth = 5;
	
	private JLabel[] labels = {
			new JLabel(	"CodePageNumber"),
			new JLabel(	"DiskFormatCode"),
			new JLabel(	"DisplayStandardCode"),
			new JLabel(	"CharacterCodeTableNumber"),
			new JLabel(	"LanguageCode"),
			new JLabel(	"OriginalProgrammeTitle"),
			new JLabel(	"OriginalEpisodeTitle"),
			new JLabel(	"TranslatedProgrammeTitle"),
			new JLabel(	"TranslatedEpisodeTitle"),
			new JLabel(	"TranslatorsName"),
			new JLabel(	"TranslatorsContactDetails"),
			new JLabel(	"SubtitleListReferenceCode"),
			new JLabel(	"CreationDate"),
			new JLabel(	"RevisionDate"),
			new JLabel(	"RevisionNumber"),
			new JLabel(	"TotalTtiBlocks"),
			new JLabel(	"TotalSubtitles"),
			new JLabel(	"TotalSubtitleGroups"),
			new JLabel(	"MaxDisplayableCharactersInRow"),
			new JLabel(	"MaximumDisplayableRows"),
			new JLabel(	"TimeCodeStatus"),
			new JLabel(	"TimeCodeStartOfProgramme"),
			new JLabel(	"TimeCodeFirstInCue"),
			new JLabel(	"TotalDisks"),
			new JLabel(	"DiskSequenceNumber"),
			new JLabel(	"CountryOfOrigin"),
			new JLabel(	"Publisher"),
			new JLabel(	"EditorsName"),
			new JLabel(	"EditorsContactDetails"),
			new JLabel(	"SpareBytes"),
			new JLabel(	"UserDefinedArea")
	};
	
	private JTextField[] textFields = new JTextField[31];	

	private static final int CodePageNumber = 0;
	private static final int DiskFormatCode = 1;
	private static final int DisplayStandardCode = 2;
	private static final int CharacterCodeTableNumber = 3;
	private static final int LanguageCode = 4;
	private static final int OriginalProgrammeTitle = 5;
	private static final int OriginalEpisodeTitle = 6;
	private static final int TranslatedProgrammeTitle = 7;
	private static final int TranslatedEpisodeTitle = 8;
	private static final int TranslatorsName = 9;
	private static final int TranslatorsContactDetails = 10;
	private static final int SubtitleListReferenceCode = 11;
	private static final int CreationDate = 12;
	private static final int RevisionDate = 13;
	private static final int RevisionNumber = 14;
	private static final int TotalTtiBlocks = 15;
	private static final int TotalSubtitles = 16;
	private static final int TotalSubtitleGroups = 17;
	private static final int MaxDisplayableCharactersInRow = 18;
	private static final int MaximumDisplayableRows = 19;
	private static final int TimeCodeStatus = 20;
	private static final int TimeCodeStartOfProgramme = 21;
	private static final int TimeCodeFirstInCue = 22;
	private static final int TotalDisks = 23;
	private static final int DiskSequenceNumber = 24;
	private static final int CountryOfOrigin = 25;
	private static final int Publisher = 26;
	private static final int EditorsName = 27;
	private static final int EditorsContactDetails = 28;
	private static final int SpareBytes = 29;
	private static final int UserDefinedArea = 30;

	/**
	 *  Constructor method
	 */
	public GSIPanel2() {		
		BoxLayout layout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
		this.setLayout(layout);
		this.setBackground(BulletApp.panelCol);
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
		
		for(int i = 0; i < textFields.length; i++) {
			textFields[i] = new JTextField(fieldWidth);
			textFields[i].setEditable(false);
			add(labels[i]);
			add(textFields[i]);
			add(new JLabel(" "));
			labels[i].setFont(BulletApp.labFont);
			labels[i].setForeground(BulletApp.labCol);
		}
	}
	
	/**
	 *  Initialise textfield values from GSIData object
	 * @param data
	 */
	public void initGSIPanel2(GSIData data) {
		textFields[CodePageNumber].setText(data.getCodePageNumber());
		textFields[DiskFormatCode].setText(data.getDiskFormatCode());
		textFields[DisplayStandardCode].setText(data.getDisplayStandardCode());
		textFields[CharacterCodeTableNumber].setText(data.getCharacterCodeTableNumber());
		textFields[LanguageCode].setText(data.getLanguageCode());
		textFields[OriginalProgrammeTitle].setText(data.getOriginalProgrammeTitle());
		textFields[OriginalEpisodeTitle].setText(data.getOriginalEpisodeTitle());
		textFields[TranslatedProgrammeTitle].setText(data.getTranslatedProgrammeTitle());
		textFields[TranslatedEpisodeTitle].setText(data.getTranslatedEpisodeTitle());
		textFields[TranslatorsName].setText(data.getTranslatorsName());
		textFields[TranslatorsContactDetails].setText(data.getTranslatorsContactDetails());
		textFields[SubtitleListReferenceCode].setText(data.getSubtitleListReferenceCode());
		textFields[CreationDate].setText(data.getCreationDate());
		textFields[RevisionDate].setText(data.getRevisionDate());
		textFields[RevisionNumber].setText(data.getRevisionNumber());
		textFields[TotalTtiBlocks].setText(data.getTotalTtiBlocks());
		textFields[TotalSubtitles].setText(data.getTotalSubtitles());
		textFields[TotalSubtitleGroups].setText(data.getTotalSubtitleGroups());
		textFields[MaxDisplayableCharactersInRow].setText(data.getMaxDisplayableCharactersInRow());
		textFields[MaximumDisplayableRows].setText(data.getMaximumDisplayableRows());
		textFields[TimeCodeStatus].setText(data.getTimeCodeStatus());
		textFields[TimeCodeStartOfProgramme].setText(data.getTimeCodeStartOfProgramme());
		textFields[TimeCodeFirstInCue].setText(data.getTimeCodeFirstInCue());
		textFields[TotalDisks].setText(data.getTotalDisks());
		textFields[DiskSequenceNumber].setText(data.getDiskSequenceNumber());
		textFields[CountryOfOrigin].setText(data.getCountryOfOrigin());
		textFields[Publisher].setText(data.getPublisher());
		textFields[EditorsName].setText(data.getEditorsName());
		textFields[EditorsContactDetails].setText(data.getEditorsContactDetails());
		textFields[SpareBytes].setText(data.getSpareBytes());
		textFields[UserDefinedArea].setText(data.getUserDefinedArea());
	}
	
}
