package rossmills99.Bullet2;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

public class CommentPanel extends JPanel {
	
	private JLabel commentTextField;
	private static final Dimension panelSize = new Dimension(SubtitleManager.SUBTITLE_WIDTH, 30);
	
	/**
	 * Create the panel.
	 */
	public CommentPanel(String commentText) {
		setBorder(new CompoundBorder(new LineBorder(new Color(255, 255, 255)), new EmptyBorder(5, 10, 5, 10)));
		setBackground(Color.PINK);
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		commentTextField = new JLabel();
		commentTextField.setBackground(Color.PINK.darker());
		add(commentTextField);
		commentTextField.setPreferredSize( panelSize );	
		commentTextField.setText(commentText);
	}
}
