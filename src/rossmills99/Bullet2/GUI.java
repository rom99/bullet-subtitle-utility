package rossmills99.Bullet2;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GUI extends JFrame implements ActionListener {
	
	JPanel mainPanel;
	
	private static final int debug = 1;
	private static final String TAG = "GUI";
	private static final String APPLICATION_TITLE = "Bullet 2.0 - subtitle utility";
		
	private GSIPanel gsiPanel;
	private GSIPanel2 gsiPanel2;
	private JScrollPane gsiScroller;
	private GlassPane glassPane;
	
	private String currentFileString = "<file>";
	private final String fullPathPreamble = "Double click copies full path to clipboard. Path: ";
	private String fullPathString = fullPathPreamble + "<path>";
	private JLabel fileNameLabel = new JLabel(currentFileString);
	
	private JList list;
	private DefaultListModel listModel = new DefaultListModel();
	private JScrollPane scroller;	
	private JLabel subtitleStatus;
	
	private JButton showHideGSI;
	
	// Menu Items
	private JMenuItem mntmOpen = new JMenuItem("Open\u2026");
	private JMenuItem mntmSave = new JMenuItem("Save");
	private JMenuItem mntmSaveAs = new JMenuItem("Save as\u2026");
	private JMenuItem mntmClose = new JMenuItem("Close");
	private JMenuItem mntmExit = new JMenuItem("Quit");
//	private JMenuItem mntmHowToUse = new JMenuItem("How to use Bullet");
	private JMenuItem mntmAbout = new JMenuItem("About");
//	private final JMenuItem mntmNewWindow = new JMenuItem("New Window");
	
	// File chooser
	private JFileChooser chooser;
	
	public void clearSubtitles() {
		listModel.clear();
	}
	
	public void addSubtitle(JPanel panel) {
		listModel.addElement(panel);
	}
	
	public void setStatus(String status) {
        subtitleStatus.setText(status);
    }
	
	public void setFields(String progName, String progNum, String spoolNum) {
		gsiPanel.setFields(progName, progNum, spoolNum);
	}
	
	public String[] getFields() {
		return gsiPanel.getFields();
	}
	
	public void setFileName(File file) {
		currentFileString = (file.getName() );
		try {
			fullPathString = file.getCanonicalPath();
		} catch (IOException e) {
			BulletApp.out(debug, "Something is wrong with the canonical path for the selected file, " +
					"			\nfilename operations might not work as expected.");
		}
		fileNameLabel.setText(currentFileString);
		fileNameLabel.setToolTipText(fullPathPreamble+fullPathString);
	}
	
	public void initGSIPanel2(GSIData data) {
		gsiPanel2.initGSIPanel2(data);
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		
		// Menus setup
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);	
		
		mntmOpen.addActionListener(this);
//		mntmNewWindow.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK) );
//		
//		mnFile.add(mntmNewWindow);
//		
//		JSeparator separator = new JSeparator();
//		mnFile.add(separator);
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		mnFile.add(mntmOpen);
		
		mnFile.add(new JSeparator());
		
		mntmSave.addActionListener(this);
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		mnFile.add(mntmSave);
		
		mntmSaveAs.addActionListener(this);
		mntmSaveAs.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		mnFile.add(mntmSaveAs);
		
		mnFile.add(new JSeparator() );
		
		mntmClose.addActionListener(this);
		mntmClose.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		mnFile.add(mntmClose);

		mnFile.add(new JSeparator());
		
		mntmExit.addActionListener(this);
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		mnFile.add(mntmExit);
		
		JMenu mnHelp = new JMenu("About");
		mnHelp.addActionListener(this);
		menuBar.add(mnHelp);
		
//		mntmHowToUse.addActionListener(this);
//		mnHelp.add(mntmHowToUse);
		
		mntmAbout.addActionListener(this);
		mnHelp.add(mntmAbout);
		
		// Main GUI elements
		glassPane = new GlassPane();
		
		gsiPanel2 = new GSIPanel2();
		gsiScroller = new JScrollPane(gsiPanel2);
		Border scrollerOuterBorder = (BorderFactory.createCompoundBorder(
				BorderFactory.createEmptyBorder(8,8,8,8),
				BorderFactory.createTitledBorder("General Subtitle Information") ) );
		gsiScroller.setBorder(BorderFactory.createCompoundBorder(
				scrollerOuterBorder,
				BorderFactory.createEmptyBorder(10,10,10,0)
				));
		
		gsiScroller.setBackground(BulletApp.panelCol);

		subtitleStatus = new JLabel();
		subtitleStatus.setForeground(Color.gray);
		
		list = new JList(listModel);
		list.setCellRenderer(new SubtitleRenderer() );
		
		scroller = new JScrollPane(list);
		scroller.setPreferredSize(new Dimension(560, 700));
    	scroller.setColumnHeaderView(subtitleStatus);
    	scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    	scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        gsiPanel = new GSIPanel();

        fileNameLabel.setToolTipText(fullPathString);
		fileNameLabel.setFont(new Font("Arial", Font.BOLD, 16));
		fileNameLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		fileNameLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if(e.getClickCount() > 1) {
					// on double click, copy fullPathString to system clipboard
					StringSelection selection = new StringSelection(fullPathString);
					Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
					c.setContents(selection, selection);
					//System.out.println("Doubleclicked");
				}
			}
		}
		);

        mainPanel = new JPanel(true);
		GroupLayout layout = new GroupLayout(mainPanel);
		mainPanel.setLayout(layout);
		setGlassPane(glassPane);
		Border titleBorder = BorderFactory.createLineBorder(BulletApp.frameCol);
		
        mainPanel.setBorder(BorderFactory.createCompoundBorder(
        		BorderFactory.createEmptyBorder(10, 10, 10, 10),
        		BorderFactory.createTitledBorder( titleBorder, 
        					"Bullet 2.0, 2012", 
        					TitledBorder.TRAILING, 
        					TitledBorder.BELOW_BOTTOM, 
        					new Font("Geneva", Font.ITALIC, 12), 
        					Color.darkGray) 
        		) );
        mainPanel.setBackground(BulletApp.frameCol);
        
        this.setContentPane(mainPanel);
        this.setTitle(APPLICATION_TITLE);
		
		layout.setHonorsVisibility(false);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		layout.setHorizontalGroup(
				layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup()
							.addComponent(fileNameLabel, GroupLayout.Alignment.LEADING)
							.addComponent(gsiPanel, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE)			
							.addComponent(gsiScroller, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE)
					)						
					.addComponent(scroller)
				);
		layout.setVerticalGroup(
				layout.createSequentialGroup()
				.addComponent(fileNameLabel)
				.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
								.addComponent(gsiPanel)
								.addComponent(gsiScroller)
						)
				.addComponent(scroller)
				));
		
        chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("STL files", "stl");
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setFileFilter(filter);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
				
//		if(o == mntmNewWindow) {
//			System.out.println("NewWindow selected: "); 
//			BulletApp.openNewWindow();
//		}
		
		if(o == mntmOpen) {
			gsiPanel.toggleFields();
			int returnVal = chooser.showOpenDialog(this);
            if(returnVal == JFileChooser.APPROVE_OPTION) {  
            	setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            	BulletApp.openFile(chooser.getSelectedFile() );            	
            }
            gsiPanel.toggleFields();
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
		
		if(o == mntmSave) {
//			System.out.println("Save selected...");
			BulletApp.saveFile();
		}
		
		if(o== mntmSaveAs) {
			int returnVal = chooser.showSaveDialog(this);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
//				System.out.println("Save as selected...");
				BulletApp.saveAsFile(chooser.getSelectedFile() ); 
			} 
//			else System.out.println("Save as cancelled");
		}
		
		if(o== mntmClose) {
			BulletApp.exit();
		}
		
		if(o== mntmExit) {
			BulletApp.exit();
		}
		
//		if(o== mntmHowToUse) {
//		
//		}
		
		if(o== mntmAbout) {
			HelpDialog helpDialog = new HelpDialog(this, true);
			helpDialog.setLocationRelativeTo(this);
			helpDialog.setVisible(true);
		}
	}
}
