/**
 * 
 */
package rossmills99.Bullet2;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;

/** A panel to display GSI (General Subtitle Information) fields
 * @author rossmills
 *
 */
public class GSIPanel extends JPanel {
	private JFormattedTextField progNameField;
	private JFormattedTextField progNumField;
	private JFormattedTextField spoolNumField;	
	
	public GSIPanel() {
		super();
		Border outerBorder = (BorderFactory.createCompoundBorder(
				BorderFactory.createEmptyBorder(8,8,8,8),
				BorderFactory.createTitledBorder("Header info") ) );
		setBorder(BorderFactory.createCompoundBorder(
				outerBorder,
				BorderFactory.createEmptyBorder(5,5,5,5)
				));
		setBackground(BulletApp.panelCol);
		
		progNameField = new JFormattedTextField();
		progNameField.setDocument(new JTextFieldLimit(32));
	
		progNumField = new JFormattedTextField();
		progNumField.setDocument(new JTextFieldLimit(16));
		
		spoolNumField = new JFormattedTextField();
		spoolNumField.setDocument(new JTextFieldLimit(32));
		
		JLabel lblSpoolNumber = new JLabel("Spool number:");
		lblSpoolNumber.setFont(BulletApp.labFont);
		lblSpoolNumber.setForeground(BulletApp.labCol);
		
		JLabel lblProgrammeName = new JLabel("Programme name:");
		lblProgrammeName.setFont(BulletApp.labFont);
		lblProgrammeName.setForeground(BulletApp.labCol);
		
		JLabel lblProgrammeNumber = new JLabel("Programme number:");
		lblProgrammeNumber.setFont(BulletApp.labFont);
		lblProgrammeNumber.setForeground(BulletApp.labCol);	
		
		// GSI Panel layout
		GroupLayout layout = new GroupLayout(this);		
		setLayout(layout);
		
		layout.setHorizontalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGap(10)
					.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
							.addGroup(layout.createSequentialGroup()
								.addGap(6)
								.addComponent(progNumField))
							.addComponent(lblProgrammeName, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
								.addGap(6)
								.addComponent(progNameField, GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE))
							.addComponent(lblProgrammeNumber, GroupLayout.Alignment.LEADING)
							.addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
								.addGap(6)
								.addComponent(spoolNumField))
							.addComponent(lblSpoolNumber, GroupLayout.Alignment.LEADING)
								)
							)
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGap(10)
					.addComponent(lblProgrammeName)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(progNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(lblProgrammeNumber)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(progNumField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(lblSpoolNumber)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(spoolNumField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
	}
	
	public void toggleFields() {
		progNameField.setEnabled(!progNameField.isEnabled() );
		progNumField.setEnabled(!progNumField.isEnabled() );
		spoolNumField.setEnabled(!spoolNumField.isEnabled() );
	}
	
	public void setFields(String progName, String progNum, String spoolNum) {
		progNameField.setText(progName);
		progNumField.setText(progNum);
		spoolNumField.setText(spoolNum);
	}
	
	public String[] getFields() {
		String[] fieldValues = { progNameField.getText(),
								progNumField.getText(),
								spoolNumField.getText()
		};
		
		return fieldValues;
	}	
}
