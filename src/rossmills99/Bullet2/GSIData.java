package rossmills99.Bullet2;
import java.io.IOException;
import java.util.Arrays;

/**
 *  Stores all details from GSI Data fields and provides getters and setters.
 *  
 *  @author rossmills
 *  @version 25/02/2012
 */

public class GSIData {
	
	private static final int debug = 1;
	private static final String TAG = "GSIData";
	
	private String CodePageNumber;  			// 3 bytes
	private String DiskFormatCode; 				// 8 bytes
	private String DisplayStandardCode; 		// 1 bytes
	private String CharacterCodeTableNumber;	// 2 bytes
	private String LanguageCode; 				// 2 bytes
	private String OriginalProgrammeTitle; 		// 32 bytes
	private String OriginalEpisodeTitle;  		// 32 bytes
	private String TranslatedProgrammeTitle;	// 32 bytes 
	private String TranslatedEpisodeTitle;  	// 32 bytes 
	private String TranslatorsName; 			// 32 bytes 
	private String TranslatorsContactDetails; 	// 32 bytes 
	private String SubtitleListReferenceCode; 	// 16 bytes 
	private String CreationDate; 				// 6 bytes 
	private String RevisionDate; 				// 6 bytes 
	private String RevisionNumber; 				// 2 bytes 
	private String TotalTtiBlocks;   			// 5 bytes 
	private String TotalSubtitles; 				// 5 bytes 
	private String TotalSubtitleGroups; 		// 3 bytes 
	private String MaxDisplayableCharactersInRow; // 2 bytes  
	private String MaximumDisplayableRows; 		// 2 bytes 
	private String TimeCodeStatus; 				// 1 bytes 
	private String TimeCodeStartOfProgramme; 	// 8 bytes  
	private String TimeCodeFirstInCue; 			// 8 bytes 
	private String TotalDisks; 					// 1 bytes 
	private String DiskSequenceNumber; 			// 1 bytes 
	private String CountryOfOrigin; 			// 3 bytes 
	private String Publisher; 					// 32 bytes 
	private String EditorsName; 				// 32 bytes 
	private String EditorsContactDetails; 		// 32 bytes 
	private String SpareBytes; 					// 75 bytes
	private String UserDefinedArea; 			// 576 bytes
										// Total:	1024 bytes
	private final int CODEPAGENUMBER = 0;
	private final int DISKFORMATCODE = 1;
	private final int DISPLAYSTANDARDCODE = 2;
	private final int CHARACTERCODETABLENUMBER = 3;
	private final int LANGUAGECODE = 4;
	private final int ORIGINALPROGRAMMETITLE = 5;
	private final int ORIGINALEPISODETITLE = 6;
	private final int TRANSLATEDPROGRAMMETITLE = 7;
	private final int TRANSLATEDEPISODETITLE = 8;
	private final int TRANSLATORSNAME = 9;
	private final int TRANSLATORSCONTACTDETAILS = 10;
	private final int SUBTITLELISTREFERENCECODE = 11;
	private final int CREATIONDATE = 12;
	private final int REVISIONDATE = 13;
	private final int REVISIONNUMBER = 14;
	private final int TOTALTTIBLOCKS = 15;
	private final int TOTALSUBTITLES = 16;
	private final int TOTALSUBTITLEGROUPS = 17;
	private final int MAXDISPLAYABLECHARACTERSINROW = 18;
	private final int MAXIMUMDISPLAYABLEROWS = 19;
	private final int TIMECODESTATUS = 20;
	private final int TIMECODESTARTOFPROGRAMME = 21;
	private final int TIMECODEFIRSTINCUE = 22;
	private final int TOTALDISKS = 23;
	private final int DISKSEQUENCENUMBER = 24;
	private final int COUNTRYOFORIGIN = 25;
	private final int PUBLISHER = 26;
	private final int EDITORSNAME = 27;
	private final int EDITORSCONTACTDETAILS = 28;
	private final int SPAREBYTES = 29;
	private final int USERDEFINEDAREA = 30;
	
//	private String[] gsiDataNames = { CodePageNumber, DiskFormatCode,
//			DisplayStandardCode, CharacterCodeTableNumber, LanguageCode,
//			OriginalProgrammeTitle, OriginalEpisodeTitle,
//			TranslatedProgrammeTitle, TranslatedEpisodeTitle,
//			TranslatorsName, TranslatorsContactDetails,
//			SubtitleListReferenceCode, CreationDate, RevisionDate,
//			RevisionNumber, TotalTtiBlocks, TotalSubtitles,
//			TotalSubtitleGroups, MaxDisplayableCharactersInRow,
//			MaximumDisplayableRows, TimeCodeStatus,
//			TimeCodeStartOfProgramme, TimeCodeFirstInCue, TotalDisks,
//			DiskSequenceNumber, CountryOfOrigin, Publisher, EditorsName,
//			EditorsContactDetails, SpareBytes, UserDefinedArea };
	
	// This maps the data from the file to the appropriate variables - for example, Code Page Number is stored in the first 3 bytes of GSI data. 
	private final short[] byteMap = { 3, 8, 1, 2, 2, 32, 32, 32, 32, 32, 32, 16, 6, 6, 2, 5, 5,
			3, 2, 2, 1, 8, 8, 1, 1, 3, 32, 32, 32, 75, 576 };

	// This is to hold the 1024 bytes of GSI data from the file.
	private byte[] gsiBytes;
	
	
	/**
	 *  Constructor method
	 * @param gsiBytes	A char array containing all of the GSI data from the stl file. This should be exactly 1024 bytes.
	 */
	public GSIData(byte[] gsiBytes) throws IOException {
		
		// Get the raw GSI data - should be 1024 bytes
		this.gsiBytes = gsiBytes;
		
		if(gsiBytes.length != 1024) {
			SubtitleManager.err("GSI data is not the expected length. The file could be corrupted.");
			throw new IOException("GSIData: gsiBytes is not the expected length");		
		}

		// There should be 30 GSI fields
		String[] gsiData = new String[byteMap.length]; 
		
		// Assign GSI data bytes to appropriate gsiData indexes
		int pointer = 0;
		for(int i = 0; i < byteMap.length; i++) {
			gsiData[i] = new String ( Arrays.copyOfRange(gsiBytes, pointer, pointer+byteMap[i] ));			
			pointer += byteMap[i];
			//System.out.println(gsiData[i]);
		}
		
		// Assign GSI data to appropriate variables
		CodePageNumber = gsiData[CODEPAGENUMBER];
		DiskFormatCode = gsiData[DISKFORMATCODE];
		DisplayStandardCode = gsiData[DISPLAYSTANDARDCODE];
		CharacterCodeTableNumber = gsiData[CHARACTERCODETABLENUMBER];
		LanguageCode = gsiData[LANGUAGECODE];
		OriginalProgrammeTitle = gsiData[ORIGINALPROGRAMMETITLE];
		OriginalEpisodeTitle = gsiData[ORIGINALEPISODETITLE];
		TranslatedProgrammeTitle = gsiData[TRANSLATEDPROGRAMMETITLE];
		TranslatedEpisodeTitle = gsiData[TRANSLATEDEPISODETITLE];
		TranslatorsName = gsiData[TRANSLATORSNAME];
		TranslatorsContactDetails = gsiData[TRANSLATORSCONTACTDETAILS];
		SubtitleListReferenceCode = gsiData[SUBTITLELISTREFERENCECODE];
		CreationDate = gsiData[CREATIONDATE];
		RevisionDate = gsiData[REVISIONDATE];
		RevisionNumber = gsiData[REVISIONNUMBER];
		TotalTtiBlocks = gsiData[TOTALTTIBLOCKS];
		TotalSubtitles = gsiData[TOTALSUBTITLES];
		TotalSubtitleGroups = gsiData[TOTALSUBTITLEGROUPS];
		MaxDisplayableCharactersInRow = gsiData[MAXDISPLAYABLECHARACTERSINROW];
		MaximumDisplayableRows = gsiData[MAXIMUMDISPLAYABLEROWS];
		TimeCodeStatus = gsiData[TIMECODESTATUS];
		TimeCodeStartOfProgramme = gsiData[TIMECODESTARTOFPROGRAMME];
		TimeCodeFirstInCue = gsiData[TIMECODEFIRSTINCUE];
		TotalDisks = gsiData[TOTALDISKS];
		DiskSequenceNumber = gsiData[DISKSEQUENCENUMBER];
		CountryOfOrigin = gsiData[COUNTRYOFORIGIN];
		Publisher = gsiData[PUBLISHER];
		EditorsName = gsiData[EDITORSNAME];
		EditorsContactDetails = gsiData[EDITORSCONTACTDETAILS];
		SpareBytes = gsiData[SPAREBYTES];
		UserDefinedArea = gsiData[USERDEFINEDAREA];
	}

	// Getter and setter methods
	
	/**
	 * @return the codePageNumber
	 */
	public String getCodePageNumber() {
		return CodePageNumber;
	}

	/**
	 * @param codePageNumber the codePageNumber to set
	 */
	public void setCodePageNumber(String codePageNumber) {
		CodePageNumber = codePageNumber;
	}

	/**
	 * @return the diskFormatCode
	 */
	public String getDiskFormatCode() {
		return DiskFormatCode;
	}

	/**
	 * @param diskFormatCode the diskFormatCode to set
	 */
	public void setDiskFormatCode(String diskFormatCode) {
		DiskFormatCode = diskFormatCode;
	}

	/**
	 * @return the displayStandardCode
	 */
	public String getDisplayStandardCode() {
		return DisplayStandardCode;
	}

	/**
	 * @param displayStandardCode the displayStandardCode to set
	 */
	public void setDisplayStandardCode(String displayStandardCode) {
		DisplayStandardCode = displayStandardCode;
	}

	/**
	 * @return the characterCodeTableNumber
	 */
	public String getCharacterCodeTableNumber() {
		return CharacterCodeTableNumber;
	}

	/**
	 * @param characterCodeTableNumber the characterCodeTableNumber to set
	 */
	public void setCharacterCodeTableNumber(String characterCodeTableNumber) {
		CharacterCodeTableNumber = characterCodeTableNumber;
	}

	/**
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return LanguageCode;
	}

	/**
	 * @param languageCode the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		LanguageCode = languageCode;
	}

	/**
	 * @return the originalProgrammeTitle
	 */
	public String getOriginalProgrammeTitle() {
		return OriginalProgrammeTitle;
	}

	/**
	 * @param originalProgrammeTitle the originalProgrammeTitle to set
	 */
	public void setOriginalProgrammeTitle(String originalProgrammeTitle) {
		OriginalProgrammeTitle = originalProgrammeTitle;
	}

	/**
	 * @return the originalEpisodeTitle
	 */
	public String getOriginalEpisodeTitle() {
		return OriginalEpisodeTitle;
	}

	/**
	 * @param originalEpisodeTitle the originalEpisodeTitle to set
	 */
	public void setOriginalEpisodeTitle(String originalEpisodeTitle) {
		OriginalEpisodeTitle = originalEpisodeTitle;
	}

	/**
	 * @return the translatedProgrammeTitle
	 */
	public String getTranslatedProgrammeTitle() {
		return TranslatedProgrammeTitle;
	}

	/**
	 * @param translatedProgrammeTitle the translatedProgrammeTitle to set
	 */
	public void setTranslatedProgrammeTitle(String translatedProgrammeTitle) {
		TranslatedProgrammeTitle = translatedProgrammeTitle;
	}

	/**
	 * @return the translatedEpisodeTitle
	 */
	public String getTranslatedEpisodeTitle() {
		return TranslatedEpisodeTitle;
	}

	/**
	 * @param translatedEpisodeTitle the translatedEpisodeTitle to set
	 */
	public void setTranslatedEpisodeTitle(String translatedEpisodeTitle) {
		TranslatedEpisodeTitle = translatedEpisodeTitle;
	}

	/**
	 * @return the translatorsName
	 */
	public String getTranslatorsName() {
		return TranslatorsName;
	}

	/**
	 * @param translatorsName the translatorsName to set
	 */
	public void setTranslatorsName(String translatorsName) {
		TranslatorsName = translatorsName;
	}

	/**
	 * @return the translatorsContactDetails
	 */
	public String getTranslatorsContactDetails() {
		return TranslatorsContactDetails;
	}

	/**
	 * @param translatorsContactDetails the translatorsContactDetails to set
	 */
	public void setTranslatorsContactDetails(String translatorsContactDetails) {
		TranslatorsContactDetails = translatorsContactDetails;
	}

	/**
	 * @return the subtitleListReferenceCode
	 */
	public String getSubtitleListReferenceCode() {
		return SubtitleListReferenceCode;
	}

	/**
	 * @param subtitleListReferenceCode the subtitleListReferenceCode to set
	 */
	public void setSubtitleListReferenceCode(String subtitleListReferenceCode) {
		SubtitleListReferenceCode = subtitleListReferenceCode;
	}

	/**
	 * @return the creationDate
	 */
	public String getCreationDate() {
		return CreationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		CreationDate = creationDate;
	}

	/**
	 * @return the revisionDate
	 */
	public String getRevisionDate() {
		return RevisionDate;
	}

	/**
	 * @param revisionDate the revisionDate to set
	 */
	public void setRevisionDate(String revisionDate) {
		RevisionDate = revisionDate;
	}

	/**
	 * @return the revisionNumber
	 */
	public String getRevisionNumber() {
		return RevisionNumber;
	}

	/**
	 * @param revisionNumber the revisionNumber to set
	 */
	public void setRevisionNumber(String revisionNumber) {
		RevisionNumber = revisionNumber;
	}

	/**
	 * @return the totalTtiBlocks
	 */
	public String getTotalTtiBlocks() {
		return TotalTtiBlocks;
	}

	/**
	 * @param totalTtiBlocks the totalTtiBlocks to set
	 */
	public void setTotalTtiBlocks(String totalTtiBlocks) {
		TotalTtiBlocks = totalTtiBlocks;
	}

	/**
	 * @return the totalSubtitles
	 */
	public String getTotalSubtitles() {
		return TotalSubtitles;
	}

	/**
	 * @param totalSubtitles the totalSubtitles to set
	 */
	public void setTotalSubtitles(String totalSubtitles) {
		TotalSubtitles = totalSubtitles;
	}

	/**
	 * @return the totalSubtitleGroups
	 */
	public String getTotalSubtitleGroups() {
		return TotalSubtitleGroups;
	}

	/**
	 * @param totalSubtitleGroups the totalSubtitleGroups to set
	 */
	public void setTotalSubtitleGroups(String totalSubtitleGroups) {
		TotalSubtitleGroups = totalSubtitleGroups;
	}

	/**
	 * @return the maxDisplayableCharactersInRow
	 */
	public String getMaxDisplayableCharactersInRow() {
		return MaxDisplayableCharactersInRow;
	}

	/**
	 * @param maxDisplayableCharactersInRow the maxDisplayableCharactersInRow to set
	 */
	public void setMaxDisplayableCharactersInRow(
			String maxDisplayableCharactersInRow) {
		MaxDisplayableCharactersInRow = maxDisplayableCharactersInRow;
	}

	/**
	 * @return the maximumDisplayableRows
	 */
	public String getMaximumDisplayableRows() {
		return MaximumDisplayableRows;
	}

	/**
	 * @param maximumDisplayableRows the maximumDisplayableRows to set
	 */
	public void setMaximumDisplayableRows(String maximumDisplayableRows) {
		MaximumDisplayableRows = maximumDisplayableRows;
	}

	/**
	 * @return the timeCodeStatus
	 */
	public String getTimeCodeStatus() {
		return TimeCodeStatus;
	}

	/**
	 * @param timeCodeStatus the timeCodeStatus to set
	 */
	public void setTimeCodeStatus(String timeCodeStatus) {
		TimeCodeStatus = timeCodeStatus;
	}

	/**
	 * @return the timeCodeStartOfProgramme
	 */
	public String getTimeCodeStartOfProgramme() {
		return TimeCodeStartOfProgramme;
	}

	/**
	 * @param timeCodeStartOfProgramme the timeCodeStartOfProgramme to set
	 */
	public void setTimeCodeStartOfProgramme(String timeCodeStartOfProgramme) {
		TimeCodeStartOfProgramme = timeCodeStartOfProgramme;
	}

	/**
	 * @return the timeCodeFirstInCue
	 */
	public String getTimeCodeFirstInCue() {
		return TimeCodeFirstInCue;
	}

	/**
	 * @param timeCodeFirstInCue the timeCodeFirstInCue to set
	 */
	public void setTimeCodeFirstInCue(String timeCodeFirstInCue) {
		TimeCodeFirstInCue = timeCodeFirstInCue;
	}

	/**
	 * @return the totalDisks
	 */
	public String getTotalDisks() {
		return TotalDisks;
	}

	/**
	 * @param totalDisks the totalDisks to set
	 */
	public void setTotalDisks(String totalDisks) {
		TotalDisks = totalDisks;
	}

	/**
	 * @return the diskSequenceNumber
	 */
	public String getDiskSequenceNumber() {
		return DiskSequenceNumber;
	}

	/**
	 * @param diskSequenceNumber the diskSequenceNumber to set
	 */
	public void setDiskSequenceNumber(String diskSequenceNumber) {
		DiskSequenceNumber = diskSequenceNumber;
	}

	/**
	 * @return the countryOfOrigin
	 */
	public String getCountryOfOrigin() {
		return CountryOfOrigin;
	}

	/**
	 * @param countryOfOrigin the countryOfOrigin to set
	 */
	public void setCountryOfOrigin(String countryOfOrigin) {
		CountryOfOrigin = countryOfOrigin;
	}

	/**
	 * @return the publisher
	 */
	public String getPublisher() {
		return Publisher;
	}

	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher) {
		Publisher = publisher;
	}

	/**
	 * @return the editorsName
	 */
	public String getEditorsName() {
		return EditorsName;
	}

	/**
	 * @param editorsName the editorsName to set
	 */
	public void setEditorsName(String editorsName) {
		EditorsName = editorsName;
	}

	/**
	 * @return the editorsContactDetails
	 */
	public String getEditorsContactDetails() {
		return EditorsContactDetails;
	}

	/**
	 * @param editorsContactDetails the editorsContactDetails to set
	 */
	public void setEditorsContactDetails(String editorsContactDetails) {
		EditorsContactDetails = editorsContactDetails;
	}

	/**
	 * @return the spareBytes
	 */
	public String getSpareBytes() {
		return SpareBytes;
	}

	/**
	 * @param spareBytes the spareBytes to set
	 */
	public void setSpareBytes(String spareBytes) {
		SpareBytes = spareBytes;
	}

	/**
	 * @return the userDefinedArea
	 */
	public String getUserDefinedArea() {
		return UserDefinedArea;
	}

	/**
	 * @param userDefinedArea the userDefinedArea to set
	 */
	public void setUserDefinedArea(String userDefinedArea) {
		UserDefinedArea = userDefinedArea;
	}
}