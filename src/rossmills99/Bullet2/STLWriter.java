/**
 * 
 */
package rossmills99.Bullet2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;

/**
 * @author rossmills
 * @version 1.1		Adapted from GSIEditor for Bullet
 * 
 * Writes new programme name, programme number and spool number info to the GSI block as well as
 * into the Textarea of the first TTI block (first subtitle contains this info as industry standard)
 * using RandomAccessFile.
 * 
 */
public class STLWriter {
	
	private byte[] pName = new byte[32];          
	private byte[] pNo = new byte[16];
	private byte[] sNo = new byte[32];
	private byte[] headerSubtitle = new byte[112];

	private String progNameString;
	private String progNoString;
	private String spoolNoString;
	
	File outFile;
	RandomAccessFile raf;
	
	public STLWriter( String progName, String progNo, String spoolNo) {
		prepHeaderInfo(progName, progNo, spoolNo);
	}
		

//		System.out.println(new String(pName) + " " + new String(pNo) + " " + new String(sNo) );
//		System.out.println(pName.length+" "+pNo.length+" "+sNo.length);

//		if( inFile.compareTo( outFile ) < 0) {
//			//                       System.out.println("COPYING FILE");
//			copyFile( inFile, outFile );
//		} 
//                   else System.out.println("NO COPY");
	
	
	public void writeFile(File outFile) {
		/* Open file for writing */
		try {
			raf = new RandomAccessFile( outFile, "rw");
			writeHeaderInfo();
			raf.close();

		} catch (IOException ex) {
		
			System.out.println("Error instantiating RandomAccessFile: " + ex.getLocalizedMessage() );
		}
	}
	
	/**
	 *  Copies sourceFile to destFile and creates a new file if one does not exist already
	 */    
	public void copyFile(File sourceFile, File destFile) throws IOException {
		if(!destFile.exists() ) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size() );
		}
		finally {
			if(source != null) {
				source.close();
			}
			if(destination != null) {
				destination.close();
			}
		}
	}

	/**
	 * Prepare new header info
	 * 
	 *  @param	progName, progNo, spoolNo	Header info as strings
	 */
	private void prepHeaderInfo(String progName, String progNo, String spoolNo) {
		try {                   
			progNameString = progName;
			progNoString = progNo;
			spoolNoString = spoolNo;                    

			/* Add spaces */
			while (progNameString.length() < 32) {
				progNameString += " ";
			}

			while (progNoString.length() < 16) {
				progNoString += " ";
			}

			while (spoolNoString.length() < 32) {
				spoolNoString += " ";
			}

			/* Allocate byte arrays */
			pName = progNameString.substring(0, 32).getBytes("ISO-8859-1");   
			pNo = progNoString.substring(0, 16).getBytes("ISO-8859-1");
			sNo = spoolNoString.substring(0, 32).getBytes("ISO-8859-1");
			
			headerSubtitle = formatHeaderSubtitle(pName, pNo, sNo);

		} catch (UnsupportedEncodingException ex) {
			System.out.println("STLWriter: Unsupported Encoding: " + ex.getLocalizedMessage() );
		}
	}
	
	/**
	 *  Write the bytes to the file
	 */
	private void writeHeaderInfo() {
		writeBytes(pName, 16);
		writeBytes(pNo, 208);
		writeBytes(sNo, 48);
		writeBytes(headerSubtitle, 1040);
	}
	
	

   /**
    *  Writes the byte array <code> bytes</code> starting at position <code>from</code>
    */
   
   private void writeBytes(byte[] bytes, long from) {
       try {
           raf.seek(from);
           raf.write(bytes);
           
       } catch (IOException ex) {
           System.out.println("STLWriter: File write error: " + ex.getLocalizedMessage() );
       }
   }


   /**
    *  Given byte arrays for the programme name (pName), number (pNo) and spool number (sNo),
    *  builds a new header subtitle with the relevant control characters inserted and returns
    *  as a byte array.
    */
   private byte[] formatHeaderSubtitle(byte[] pName, byte[] pNo, byte[] sNo) {
       byte[] bytes = new byte[112];
       
       // 0x0D = double height
       bytes[0] = 0x0D;
       
       // 0x0B = start box
       bytes[1] = 0x0B;
       bytes[2] = 0x0B;
       
       // Counts position within the bytes array (just makes things easier to keep a count)
       int byteCounter = 3;
       
       // Row length (40) minus prog name length, divide by two, to center
       
       for(int i = 0; i < pName.length; i++){
           bytes[byteCounter] = pName[i];
           byteCounter++;
       }
       
       bytes[byteCounter++] = 0x0A;
       bytes[byteCounter++] = 0x0A;
       bytes[byteCounter++] = (byte)0x8A;
       bytes[byteCounter++] = (byte)0x8A;        
       bytes[byteCounter++] = 0x0D;
       bytes[byteCounter++] = 0x0B;
       bytes[byteCounter++] = 0x0B;               
       
       for(int i = 0; i < pNo.length; i++) {
           bytes[byteCounter] = pNo[i];
           byteCounter++;
       }
       
       bytes[byteCounter++] = 0x0A;
       bytes[byteCounter++] = 0x0A;
       bytes[byteCounter++] = (byte)0x8A;
       bytes[byteCounter++] = (byte)0x8A;        
       bytes[byteCounter++] = 0x0D;
       bytes[byteCounter++] = 0x0B;
       bytes[byteCounter++] = 0x0B; 
       
       for(int i = 0; i < sNo.length; i++) {
           bytes[byteCounter] = sNo[i];
           byteCounter++;
       }
       
       bytes[byteCounter++] = 0x0A;
       bytes[byteCounter++] = 0x0A;
       bytes[byteCounter++] = (byte)0x8A;
       bytes[byteCounter++] = (byte)0x8A;
       
       for( ; byteCounter < bytes.length; byteCounter++) {
           bytes[byteCounter] = (byte)0x8F;
       }
       
       return bytes;      
       
   }

	/**
	 *  Removes trailing spaces from a string
	 */
//  private String removeSpaces(String text) {
//      String newText = "";
//      String spaces = "";
//      for(int i = 0; i < text.length(); i++) {
//          if(text.charAt(i) == ' ') {
//              spaces += text.charAt(i);
//          }
//          else {
//              newText += spaces;
//              newText += text.charAt(i);
//              spaces = "";
//          }
//      }
//      return newText;
//  }
//  
//  
    

}
