package rossmills99.Bullet2;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * GSIEditor accesses the GSI information in an EBU-STL subtitle file and allows you to overwrite 
 * the GSI information with new values, or save to a new file.
 * 
 * New GUI and logic for version 3.0.
 *  
 * @ Ross Mills
 * @ Version 3.0, 25.02.2012
 */

public class SubtitleManager {
	
	private static final int debug = 1;
	private static final String TAG = "SubtitleManager";
	
	public static final int SUBTITLE_WIDTH = 350; 
	
	/**
	 * A Graphic User Interface for this instance of SubtitleManager
	 */
	private GUI gui;

    /**
     *  The current file
     */
	private File file;
    
	/**
	 *  The GSI block - General Subtitle Information
	 */
	private GSIData GSI;
	
	/**
	 *  An arraylist of TTI blocks - Text and Timing Information
	 */
	private ArrayList<TTIData> TTIs = new ArrayList<TTIData>();
	
	/**
	 *  An arraylist of SubtitlePanels for display
	 */
	private ArrayList<JPanel> subtitlePanels = new ArrayList<JPanel>();
	
    /**
     *  Initial size of window
     */
    private static final Rectangle winSize = new Rectangle(900,750);
    
    /**
     *  The x and y co-ordinates for the initial location of the window
     */
    private static final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    
    private static final int winPositionX = (int) ( (screenSize.getWidth() / 2) - (winSize.getWidth() / 2) ) ;
    private static final int winPositionY = (int) ( (screenSize.getHeight() / 2) - (winSize.getHeight() / 2) ) ;   
    
    /**
     *  SubtitleManager constructor method - initialises GUI
     */
    public SubtitleManager() {
    	gui = new GUI();
        gui.setBounds(winSize);
        gui.setLocation(winPositionX, winPositionY);
        gui.addWindowListener( 
            new WindowAdapter() {
                public void windowClosing( WindowEvent event) {
                    BulletApp.windowClosing(SubtitleManager.this);                 
                }
            });
        gui.setVisible( true );
    }
    
    public void closeWindow() {
    	gui.setVisible(false);
    }    
        
    public static void err(String message) {
    	JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
      
   /** 
    *  Opens a new file in this SubtitleManager's window
    *  
    *  @param e     The event
    */
    public void openFile(File theFile) {
    	file = theFile;
    	
    	// Clear all TTIs first
    	TTIs.clear();   	
    	
    	// Read file
    	STLReader reader = new STLReader( file );    	
    	try {
    		// Get GSI data
			GSI = new GSIData(reader.getGSIBytes() );
			// Get raw TTIBlocks
			ArrayList<byte[]> TTIBlocks = reader.getTTIs();
			// Create a list of TTI objects from the TTIBlocks
			for (byte[] TTIBlock : TTIBlocks) {
				TTIData temp = new TTIData(TTIBlock);
				TTIs.add(temp);
			}			
		} catch (IOException e) {
			e.printStackTrace();
		}    	
    	
    	// Generate subtitles from the TTIs
    	subtitlePanels = SubtitleConverter.getSubtitlesFromTTIs(TTIs);
    	displaySubtitles();
//    	System.out.println("CCT: " + GSI.getCharacterCodeTableNumber() );
    }
    
    /**
     *  Saves the file in this SubtitleManager's window
     */
    public void saveFile() {
    	if(file != null) {
    		String[] fields = gui.getFields();
    		STLWriter writer = new STLWriter(fields[0], fields[1], fields[2]);
    		writer.writeFile(file);
    		openFile(file);
//    		System.out.println("File saved");
    	} else {
    		JOptionPane.showMessageDialog(gui, "Please open a file before saving", "Message", JOptionPane.ERROR_MESSAGE);
    	}
    }
    
    public void saveAsFile(File newFile) {
    	if(newFile != null && file != null) {
	    	String[] fields = gui.getFields(); // Gets prog name, prog num, spool num
	    	STLWriter writer = new STLWriter(fields[0], fields[1], fields[2]);
//	    	System.out.println("FileComparToNewFile: " + file.compareTo(newFile) );
	    	if(!file.equals(newFile) ) {
	    		try {
	    			writer.copyFile(file, newFile);
	    		} catch (IOException e) {
	    			System.out.println("SubtitleManager: STLWriter: Error copying file: " + e.getLocalizedMessage() );
	    		}
	    	}
	    	writer.writeFile(newFile);
	    	file = newFile;
//	    	System.out.println("File saved as: " + file.getName() );
	    	gui.setFileName(file);
	    	openFile(file);
    	} else {
    		JOptionPane.showMessageDialog(gui, "Please open a file before saving", "Message", JOptionPane.ERROR_MESSAGE);
    	}
    }
    
    private void displaySubtitles() {
    	// Initialise gui 	
    	gui.clearSubtitles();
    	for(JPanel subtitlePanel : subtitlePanels) {
    		gui.addSubtitle(subtitlePanel);
    	}
    	gui.setFields(GSI.getOriginalProgrammeTitle().trim(),  GSI.getSubtitleListReferenceCode().trim(), GSI.getOriginalEpisodeTitle().trim() );
    	gui.initGSIPanel2(GSI);
    	String plural = (subtitlePanels.size() == 0 || subtitlePanels.size() > 1) ? " subtitles" : " subtitle";
    	gui.setStatus("Displaying "+subtitlePanels.size() + plural);
    	gui.setFileName(file);	
    }
   
}


