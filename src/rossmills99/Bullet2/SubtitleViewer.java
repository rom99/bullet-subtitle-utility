/**
 * 
 */
package rossmills99.Bullet2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**	A scrolling panel for displaying a list of subtitles
 * 
 * @author rossmills
 *
 */
public class SubtitleViewer extends JPanel {
	
	private JLabel subtitleStatus;
	private JPanel subtitleHolder;
	
	public SubtitleViewer() {
		super();
		/* The order of declaration of the scrollpane and the subtitle viewer 
		 * seems to be important in preventing the scroller from jumping around when adding items to the panel
		 * 
		 */
		subtitleHolder = new JPanel();
        subtitleHolder.setBackground(Color.white);
        subtitleHolder.setLayout(new BoxLayout(subtitleHolder, BoxLayout.Y_AXIS) );
        subtitleHolder.setBorder(BorderFactory.createBevelBorder(2));
        
		subtitleStatus = new JLabel("Status");
		subtitleStatus.setFont(new Font( "Arial", Font.PLAIN, 14 ));
		subtitleStatus.setBorder( BorderFactory.createEmptyBorder(4, 4, 4, 4) );
		subtitleStatus.setForeground(Color.darkGray);
	
	}
	
	public void setStatus(String status) {
		subtitleStatus.setText(status);
	}
		
	public void add(JComponent component) {
		subtitleHolder.add(component);
	}
	
	public void removeAll() {
		subtitleHolder.removeAll();
	}
}
