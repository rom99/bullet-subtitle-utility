package rossmills99.Bullet2;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 */

/**
 * Reads a file, splits into GSI section and multiple TTIs. Stores as arrays of bytes.
 * 
 * @author rossmills
 *
 */
public class STLReader {
	
	private static final int debug = 1;
	private static final String TAG = "STLReader";
	
	private byte[] GSIBytes = new byte[1024];
	private ArrayList<byte[]> TTIs = new ArrayList<byte[]>();	
	
	/**
	 *  Pass the file to be read
	 * @param file	The file object
	 */
	public STLReader(File file) {
		try{			

			BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file) );
			//System.out.println("File opened, reading file...");
			stream.read(GSIBytes, 0, 1024);
			int count = 0;
//			for(byte b : GSIBytes) {				
//				System.out.printf("0x%02X ", b);
//				count++; 
//				if (count > 20) {System.out.println();count=0;}
//			}
//			System.out.println("GSI data ends--------\n");
			
			byte[] TTI = new byte[128]; // temp variable to hold each TTI as it is read
			
			while(stream.read(TTI, 0, 128) > 0) { // Read 128 bytes - the length of one TTI - and store in the temp variable TTI
				
				// Add TTI to TTIs ArrayList
				TTIs.add(TTI); 
//				count = 0;
//				for(int i = 0; i < TTI.length; i++) {				
//					//Print hex value of each TTI byte
//					System.out.printf("0x%02X ", TTI[i] );
//					if(i == 15) System.out.println("Text: ");
//					count++; 
//					if (count > 20) {System.out.println();count=0;}
//				}
//				System.out.println("END TTI BLOCK---------");
				
				TTI = new byte[128]; // Re-initialise TTI! It must point to a different array each time.
			}
//			System.out.printf("END FILE\n%S TTIs read.\n\n", TTIs.size() );			
			stream.close();
			
		} catch (IOException ioEx) {
			SubtitleManager.err("Error reading file.");
			ioEx.printStackTrace();
			return;
		}
	}
	
	public byte[] getGSIBytes() {
		return GSIBytes;
	}
	
	public ArrayList<byte[]> getTTIs() {
		return TTIs;
	}
}
