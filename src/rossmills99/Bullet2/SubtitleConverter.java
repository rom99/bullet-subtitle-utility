/**
 * 
 */
package rossmills99.Bullet2;

import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * A helper class for converting from subtitles to TTIs and vice versa
 * @author rossmills
 *
 */
public class SubtitleConverter {
	
	/** Returns a list of JPanels given a list of TTIData blocks */
	public static ArrayList<JPanel> getSubtitlesFromTTIs(ArrayList<TTIData> TTIs) {
    	ArrayList<JPanel> subtitlePanels = new ArrayList<JPanel>();
		
    	int subNo = 0;
    	byte[] timeIn = new byte[4];
    	byte[] timeOut = new byte[4];
    	String subtitleText = "";
    	boolean extending = false;
    	
    	for(TTIData TTI : TTIs) {
    		int EBN = TTI.getExtensionBlockNumber() & 0xff;	
    		
    		// If TTI is a comment, add a comment
    		if(TTI.getCommentFlag() == 0x01) {
    			subtitlePanels.add(subtitlePanels.size()-1, new CommentPanel( cleanText(TTI.getSubtitleTextField() ) ) );
    		} else
    		// If TTI is last block and not an extension block, add a new subtitle
    		if(!extending && EBN == 0xff) {
//    			System.out.println("Added Single TTI Subtitle");
    			subtitlePanels.add(new SubtitlePanel( TTI.getSubtitleNumber(),
    							TTI.getTimeCodeIn(),
    							TTI.getTimeCodeOut(),
    							cleanText(TTI.getSubtitleTextField() ) ) );
    		} else 
    		
    		// If TTI is first block of an extending set, store details for later and flag extending
    		if (EBN == 0x00 ) {
//    			System.out.print("Extending...");
    			subNo = TTI.getSubtitleNumber();
    			timeIn = TTI.getTimeCodeIn();
    			timeOut = TTI.getTimeCodeOut();
    			subtitleText = cleanText( TTI.getSubtitleTextField() );
    			extending = true;
    		} else
    		
    		// If TTI is the last in an extending block, add to textfield and add subtitle
    		if (extending && EBN == 0xff) { // -1 is 0xFF (twos' complement)
//    			System.out.println("Added Extended Subtitle");
    			subtitleText += cleanText(TTI.getSubtitleTextField() );
    			subtitlePanels.add(new SubtitlePanel( subNo,
						timeIn,
						timeOut,
						subtitleText ) );
    			extending = false; // No longer extending
       		} 
//    		else System.out.println("TTI Fail");
    	}
    	
    	return subtitlePanels;
	}
	
	private static String cleanText(byte[] text) {
		
		// newline character code, for comparison
		int newline = 0x8A;		
		Charset charset = Charset.forName("ISO8859_1");
		
		String cleanText = "";
		for(byte signedByte : text) {
			int unsignedByte = (int) signedByte & 0xff; // Get "unsigned byte" for easier comparisons
			if( (unsignedByte > 31 && unsignedByte < 128) || (unsignedByte > 159 && unsignedByte < 256) ) {
				if(unsignedByte == 0xA4) {
					cleanText += "$";
				} else {
					byte[] b = { (byte) unsignedByte }; // cast back to byte, put in array
					cleanText += new String(b, charset); // add to string, convert using appropriate charset
				}
			} else
			if (cleanText.length() > 0 ) {
				if (unsignedByte == newline && !cleanText.substring(cleanText.length()-1).equals("\n") ) {
					cleanText += "\n"; // Get newline characters
				} else				
				if (unsignedByte < 32 && !cleanText.substring(cleanText.length()-1).equals(" ") ) { // Check there wasn't a space before
					cleanText += " "; // Insert spaces for control characters
				}
			} else
			// Cases where this is the first character of the text field
			if(unsignedByte == newline) {
				cleanText += "\n";
			} else
			if(unsignedByte < 32) {
				cleanText += " ";
			}
		}
		
		return cleanText;
	}
			
//			if(c < 32) {
//				character +="<";
//				switch(c) {
//					
//				case 0:
//					character +="AK";
//					break;
//				case 1:
//					character +="AR";
//					break;
//				case 2:
//					character +="AG";
//					break;
//				case 3:
//					character +="AY";
//					break;
//				case 4:
//					character +="AB";
//					break;
//				case 5:
//					character +="AM";
//					break;
//				case 6:
//					character +="AC";
//					break;
//				case 7:
//					character +="AW";
//					break;
//				case 8:
//					character +="Flash";
//					break;
//				case 9:
//					character +="Steady";
//					break;
//				case 10:
//					character +="EndBox";
//					break;
//				case 11:
//					character +="StartBox";
//					break;
//				case 12:
//					character +="NormHeight";
//					break;
//				case 13:
//					character +="DoubHeight";
//					break;
//				case 14:
//					character +="DoubWidth";
//					break;
//				case 15:
//					character +="DoubSize";
//					break;
//				case 16:
//					character +="MK";
//					break;
//				case 17:
//					character +="MR";
//					break;
//				case 18:
//					character +="MG";
//					break;
//				case 19:
//					character +="MY";
//					break;
//				case 20: // !Repetition!
//					character +="MY";
//					break;
//				case 21:
//					character +="MB";
//					break;
//				case 22:
//					character +="MM";
//					break;
//				case 23:
//					character +="MC";
//					break;
//				case 24:
//					character +="MW";
//					break;
//				case 25:
//					character +="Conceal";
//					break;
//				case 26:
//					character +="Contig";
//					break;
//				case 27:
//					character +="Separated";
//					break;
//				case 28:
//					character +="Res";
//					break;
//				case 29:
//					character +="Kbg";
//					break;
//				case 30:
//					character +="Nbg";
//					break;
//				case 31:
//					character +="HoldMosaic";
//					break;
//				case 32:
//					character +="ReleaseMosaic";
//					break;				
//				}
//				character += ">";
			
}