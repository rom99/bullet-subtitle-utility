/**
 * 
 */
package rossmills99.Bullet2;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * @author rossmills
 *
 */
public class SubtitleRenderer extends JPanel implements ListCellRenderer {
	
	@Override
	public Component getListCellRendererComponent( 
			JList list,
			Object value,   // value to display
			int index,      // cell index
			boolean iss,    // is the cell selected
			boolean chf)    // the list and the cell have the focus
	{
		return (JPanel) value;
	}
}
