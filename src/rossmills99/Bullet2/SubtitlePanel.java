package rossmills99.Bullet2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Insets;
import javax.swing.border.LineBorder;

public class SubtitlePanel extends JPanel {
	
	private static final int debug = 1;
	private static final String TAG = "SubtitlePanel";
	
	private final Dimension panelSize = new Dimension(SubtitleManager.SUBTITLE_WIDTH, 120); 
	
	private final JLabel lblSubNo = new JLabel("Sub No");
	private final JLabel lblTimeIn = new JLabel("TimeIn");
	private final JLabel lblTimeOut = new JLabel("TimeOut");
	private final JTextArea subtitleTextArea = new JTextArea();
	
	public void setSubNo(String subNo) {
		lblSubNo.setFont(new Font("Arial", Font.PLAIN, 14));
		lblSubNo.setForeground(Color.DARK_GRAY);
		lblSubNo.setText(subNo );
	} 
	
	public void setTimeIn(byte[] timeIn) {
		String time = "";
		for(byte timePart : timeIn) {
			time += String.format("%02d:", timePart);		
		}
		time = time.substring(0, time.length()-1); // remove the last colon
		lblTimeIn.setFont(new Font("Courier", Font.PLAIN, 13));
		lblTimeIn.setForeground(Color.BLUE);
		
		lblTimeIn.setText(time);
	}
	
	public void setTimeOut(byte[] timeOut) {
		String time = "";
		for(byte timePart : timeOut) {
			time += String.format("%02d:", timePart);		
		}	
		time = time.substring(0,time.length()-1); // remove the last colon
		lblTimeOut.setFont(new Font("Courier", Font.PLAIN, 13));
		lblTimeOut.setForeground(Color.BLUE);
		lblTimeOut.setText(time);
	}
	
	public void setSubtitleText(String subtitleText) {
		subtitleTextArea.setForeground(Color.BLACK);
		subtitleTextArea.setBackground(Color.WHITE);
		subtitleTextArea.setMargin(new Insets(5, 5, 5, 5));
		subtitleTextArea.setLineWrap(true);
		subtitleTextArea.setWrapStyleWord(true);
		subtitleTextArea.setFont(new Font("Verdana", Font.PLAIN, 16));
		subtitleTextArea.setText(subtitleText);
	}
	
	/**
	 * Create the panel.
	 */
	public SubtitlePanel(int subNo, byte[] timeIn, byte[] timeOut, String subtitleText) {
		setSubNo(""+subNo);
		setTimeIn(timeIn);
		setTimeOut(timeOut);
		setSubtitleText(subtitleText);
		setPreferredSize(new Dimension(459, 101));
		createGUI();
	}
		
	private void createGUI() {
		lblTimeIn.setBackground(Color.WHITE);
		lblTimeOut.setBackground(Color.WHITE);
		setBackground(Color.lightGray);
		setBorder(new LineBorder(Color.WHITE));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblSubNo, Alignment.LEADING)
						.addComponent(lblTimeOut, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
						.addComponent(lblTimeIn, GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(subtitleTextArea, GroupLayout.PREFERRED_SIZE, 366, Short.MAX_VALUE)
					.addGap(33))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(subtitleTextArea, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblSubNo)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(lblTimeIn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblTimeOut)))
					.addGap(21))
		);
		setLayout(groupLayout);
	}
}
