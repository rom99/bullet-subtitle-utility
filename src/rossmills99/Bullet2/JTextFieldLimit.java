/**
 * 
 */
package rossmills99.Bullet2;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * @author rossmills
 * <a href="http://stackoverflow/questions/351951/how-to-limit-the-number-of-characters-in-jtextfield">
 * Source </a> of this code: StackOverFlow
 * 13/04/2012
 */
public class JTextFieldLimit extends PlainDocument {
	private int limit;
	
	JTextFieldLimit(int limit) {
		super();
		this.limit = limit;
	}
	
	public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
		if (str == null) return;
		
		if( (getLength() + str.length() ) <= limit) {
			super.insertString(offset,  str, attr);
		}
	}
}
