/**
 * 
 */
package rossmills99.Bullet2;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JScrollPane;

/**
 * @author rossmills
 *
 */
public class GlassPane extends JComponent {
	
	private static final int scrollerWidth = 300;
	private static final int scrollerHeight = 500;
	
	private static GSIPanel2 gsi;
	private static JScrollPane scroller;
	
	public GlassPane() {
		gsi = new GSIPanel2();
		scroller = new JScrollPane(gsi);
		scroller.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("General Subtitle Information"),
				BorderFactory.createEmptyBorder(10, 10, 10, 10) )
				);
		scroller.setBounds(50, 50, scrollerWidth, scrollerHeight);
		add(scroller);
	}
	
	protected void paintComponent(Graphics g) {
		g.setColor(new Color(0x10, 0x15, 0x1f, 0x85));
		g.fillRect(0, 0, getWidth(), getHeight() );
		scroller.repaint();
	}
	
	public void initGSIPanel2(GSIData gsiData) {
		gsi.initGSIPanel2(gsiData);
	}
}
