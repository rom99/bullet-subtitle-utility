/**
 * 
 */
package rossmills99.Bullet2;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.io.File;
import java.util.ArrayList;


/** The main Bullet application class
 * 
 * @author rossmills
 *
 */
public class BulletApp {
	
	private static final int debug = 0; // 0 is off, 1 is on
	private static final String TAG = "BulletApp";
	
	/**
	 *  Constants for GSI and TTI block sizes
	 */
	public static final int GSI_SIZE = 1024;
	public static final int TTI_SIZE = 128;
	
	/**
	 *  Constants for subtitle size
	 */
	public static final int SUBTITLE_WIDTH = 550;
	public static final int SUBTITLE_HEIGHT = 200;
	
	/**
	 *  Constants for frame colour, panel colour
	 */
	public static final Color frameCol = new Color(0xdd,0xdd,0xdd);
	public static final Color panelCol = Color.white;
	
	/**
	 *  Constants for label font and color
	 */
	public static final Font labFont = new Font("Arial", Font.PLAIN, 10);
	public static final Color labCol = Color.black;
	
	private static ArrayList<SubtitleManager> managers = new ArrayList<SubtitleManager>();

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		BulletApp bullet = new BulletApp();		
	}	
	
	/** 
	 * Initialise application 
	 */
	public BulletApp() {
		openNewWindow();
	}

	public static void saveFile() {		
//		System.out.println("Passing save to manager: ");
		managers.get(0).saveFile();
	}
	
	public static void saveAsFile(File file) {
//		System.out.println("Passing saveAs to manager: ");
		managers.get(0).saveAsFile(file);
	}
	
	public static void openFile(File theFile) {
		managers.get(0).openFile(theFile);
	}
	
	public static void windowClosing(SubtitleManager manager) {
		manager.closeWindow();
		managers.remove(manager);
		if(managers.isEmpty() ) {
			System.exit(0);
		}
	}
	
	public static void exit() {
		for(SubtitleManager manager: managers) {
			manager.closeWindow();
		}
		System.exit(0);
	}
		
	public static void openNewWindow() {
		managers.add(new SubtitleManager() );
	}
	
	public static void out(int debug, String msg) {
	  	if(debug > 0) System.out.println(msg);
	}

}
