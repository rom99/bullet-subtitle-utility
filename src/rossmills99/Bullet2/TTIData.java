package rossmills99.Bullet2;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 
 */

/**
 * Contains all of the data about a single TTI block. <br/>
 * Note: Because of the use of extension blocks, this does not neccessarily correspond to
 * one subtitle.  Some subtitles extend beyond a single TTI block.
 * 
 * @author rossmills
 * @version 25/02/2012
 */
public class TTIData {
	
	private static final int debug = 1;
	private static final String TAG = "TTIData";
//	SubtitleGroupNumber 
//	SubtitleNumber 
//	ExtensionBlockNumber 
//	CumulativeStatus
//	TimeCodeIn
//	TimeCodeOut 
//	VerticalPosition 
//	JustificationCode 
//	CommentFlag 
//	TextField
	
	// This is the order these values are stored in the file
	private byte SubtitleGroupNumber;
	private int SubtitleNumber;
	private byte ExtensionBlockNumber;
	private byte CumulativeStatus;
	private byte[] TimeCodeIn = new byte[4];
	private byte[] TimeCodeOut = new byte[4];	
	
	private byte VerticalPosition;
	private byte JustificationCode;
	private byte CommentFlag;	
	private byte[] SubtitleTextField; // 112 bytes

	private final int SUBTITLE_GROUP_NUMBER = 0;
	private final int SUBTITLE_NUMBER = 1;
	private final int EXTENSION_BLOCK_NUMBER = 2;
	private final int CUMULATIVE_STATUS = 3;
	private final int TIME_CODE_IN = 4;
	private final int TIME_CODE_OUT = 5;
	private final int VERTICAL_POSITION = 6;
	private final int JUSTIFICATION_CODE = 7;
	private final int COMMENT_FLAG = 8;
	private final int SUBTITLE_TEXTFIELD = 9;	
	
	private int[] byteMap = { 1, 2, 1, 1, 4, 4, 1, 1, 1, 112 };
	private byte[] dataBytes;
	
	/**
	 *  Constructor takes as a parameter the data for a single TTI as a byte array - 128 bytes.
	 * @param bytes
	 */
	public TTIData(byte[] bytes) {
		//System.out.printf("Raw:0x%02x",bytes[1]);
		this.dataBytes = bytes;
		byte[][] data = new byte[byteMap.length][];
		int pointer = 0;
		//System.out.print("byteMapTest: ");
		for(int i = 0; i < byteMap.length; i++) {
			data[i] = Arrays.copyOfRange(dataBytes, pointer, pointer+byteMap[i] );
			pointer += byteMap[i];
		}
		
		SubtitleGroupNumber = data[SUBTITLE_GROUP_NUMBER][0];
		SubtitleNumber = getInt( data[SUBTITLE_NUMBER] );
		ExtensionBlockNumber = data[EXTENSION_BLOCK_NUMBER] [0];
		CumulativeStatus = data[CUMULATIVE_STATUS] [0];
		TimeCodeIn = data[TIME_CODE_IN];
		TimeCodeOut = data[TIME_CODE_OUT];
		VerticalPosition = data[VERTICAL_POSITION] [0];
		JustificationCode = data[JUSTIFICATION_CODE][0];
		CommentFlag = data[COMMENT_FLAG][0];
		SubtitleTextField = data[SUBTITLE_TEXTFIELD];
		//System.out.println("SubNo: " + SubtitleNumber);
	}
		
	/** 
	 *  Create an int from two bytes
	 */
	private int getInt(byte[] bytes) {
		// Convert second byte to int, shift 8 places, and mask to prevent negative numbers
		int newShort = (int)(bytes[1] << 8) & 0xFFFF;		
		
		// Convert first byte to int, and mask to prevent negative numbers
		int newShort2 = (int) bytes[0] & 0xFF;
		
		// Logic OR combines the two numbers
		int newInt = (newShort | newShort2);
		
		return newInt;
	}

	/**
	 * @return the subtitleGroupNumber
	 */
	public byte getSubtitleGroupNumber() {
		return SubtitleGroupNumber;
	}

	/**
	 * @param subtitleGroupNumber the subtitleGroupNumber to set
	 */
	public void setSubtitleGroupNumber(byte subtitleGroupNumber) {
		SubtitleGroupNumber = subtitleGroupNumber;
	}

	/**
	 * @return the subtitleNumber
	 */
	public int getSubtitleNumber() {
		return SubtitleNumber;
	}

	/**
	 * @param subtitleNumber the subtitleNumber to set
	 */
	public void setSubtitleNumber(int subtitleNumber) {
		SubtitleNumber = subtitleNumber;
	}

	/**
	 * @return the extensionBlockNumber
	 */
	public byte getExtensionBlockNumber() {
		return ExtensionBlockNumber;
	}

	/**
	 * @param extensionBlockNumber the extensionBlockNumber to set
	 */
	public void setExtensionBlockNumber(byte extensionBlockNumber) {
		ExtensionBlockNumber = extensionBlockNumber;
	}

	/**
	 * @return the cumulativeStatus
	 */
	public byte getCumulativeStatus() {
		return CumulativeStatus;
	}

	/**
	 * @param cumulativeStatus the cumulativeStatus to set
	 */
	public void setCumulativeStatus(byte cumulativeStatus) {
		CumulativeStatus = cumulativeStatus;
	}

	/**
	 * @return the timeCodeIn
	 */
	public byte[] getTimeCodeIn() {
		return TimeCodeIn;
	}

	/**
	 * @param timeCodeIn the timeCodeIn to set
	 */
	public void setTimeCodeIn(byte[] timeCodeIn) {
		TimeCodeIn = timeCodeIn;
	}

	/**
	 * @return the timeCodeOut
	 */
	public byte[] getTimeCodeOut() {
		return TimeCodeOut;
	}

	/**
	 * @param timeCodeOut the timeCodeOut to set
	 */
	public void setTimeCodeOut(byte[] timeCodeOut) {
		TimeCodeOut = timeCodeOut;
	}

	/**
	 * @return the verticalPosition
	 */
	public byte getVerticalPosition() {
		return VerticalPosition;
	}

	/**
	 * @param verticalPosition the verticalPosition to set
	 */
	public void setVerticalPosition(byte verticalPosition) {
		VerticalPosition = verticalPosition;
	}

	/**
	 * @return the justificationCode
	 */
	public byte getJustificationCode() {
		return JustificationCode;
	}

	/**
	 * @param justificationCode the justificationCode to set
	 */
	public void setJustificationCode(byte justificationCode) {
		JustificationCode = justificationCode;
	}

	/**
	 * @return the commentFlag
	 */
	public byte getCommentFlag() {
		return CommentFlag;
	}

	/**
	 * @param commentFlag the commentFlag to set
	 */
	public void setCommentFlag(byte commentFlag) {
		CommentFlag = commentFlag;
	}

	/**
	 * @return the subtitleTextField
	 */
	public byte[] getSubtitleTextField() {
		return SubtitleTextField;
	}

	/**
	 * @param subtitleTextField the subtitleTextField to set
	 */
	public void setSubtitleTextField(byte[] subtitleTextField) {
		SubtitleTextField = subtitleTextField;
	}

}