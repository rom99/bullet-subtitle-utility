package rossmills99.Bullet2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

public class HelpDialog extends JDialog {

	private JButton button;

	/**
	 * Create the dialog.
	 */
	public HelpDialog(JFrame c, Boolean modality) {
		super(c, modality);
		
		button = new JButton("Back to Bullet");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HelpDialog.this.setVisible(false);
			}
		});
		
		JLabel lblHelpText = new JLabel("<html><h1> Bullet 2.0 </h1> <p> Bullet 2.0 supports the following:</p>\n<ul>\n<li>Open STL file and preview subtitles</li>\n\n<li>Preview GSI info</li><li>Synchronised editing of header info</li>\n<li>Save as new file</li>\n<li>Copy full file path to clipboard by double clicking path</li>\n<li>More features planned!</li>\n</ul>\n<br /><p>Designed and coded by Ross Mills, 2012. <br /> For more information, e-mail rossmills99@gmail.com<br /></p>\n</html>");
		lblHelpText.setBorder(new EmptyBorder(0, 20, 0, 20));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.CENTER)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(lblHelpText, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addComponent(button)
		);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.CENTER)
				.addGroup(groupLayout.createSequentialGroup()
				.addComponent(lblHelpText, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
				.addComponent(button)
				.addGap(10)
		)
		);
		getContentPane().setLayout(groupLayout);
		
		setBackground(Color.white);
		setBounds(0,0,550,350);

	}

}
